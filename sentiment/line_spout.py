import logging
import random
from pymongo import MongoClient
from datetime import datetime
import time
from uuid import uuid4
from faker import Factory

from pyleus.storm import Spout

log = logging.getLogger('counter')


class LineSpout(Spout):
    OUTPUT_FIELDS = ["line"]
    faker = None
    client = None

    def initialize(self):
        self.faker = Factory.create(locale='en')
        self.client = MongoClient('localhost')

    def next_tuple(self):
        # test delay
        time.sleep(1)
        # time.sleep(random.randint(1, 3))

        for i in range(0, random.randint(1, 3)):
            line = self.faker.sentence(15, 5)

            # log
            log.debug("LineSpout: " + line)
            self.client.log.text.insert(dict(text=line, at=datetime.now()))

            # emit
            self.emit((line,), tup_id=str(uuid4()))


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.DEBUG,
        filename='/tmp/word_count_lines.log',
        format="%(message)s",
        filemode='a',
    )

    LineSpout().run()
