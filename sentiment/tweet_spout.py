import sys, traceback
import logging
from pymongo import MongoClient
from pymongo import errors as pyerrors
from datetime import datetime
import time
from uuid import uuid4
from faker import Factory
import tweepy
import hashlib as hl

from pyleus.storm import Spout

log = logging.getLogger('counter')


class TweetSpout(Spout):
    OUTPUT_FIELDS = ["tweet", "id"]
    faker = None
    client = None
    api = None

    def initialize(self):
        self.faker = Factory.create(locale='en')
        self.client = MongoClient('localhost')

        # configuring tweepy
        consumer_key = "DCKZ5Gh1pn1adwAveIDXgWQ97"
        consumer_secret = "QAttiiCn5prm6vmLEiMcvoYk5uFyKiRLzVGCrfzQXvJofYHJ0X"
        access_token = "3236698228-kFQAdKvSHaC9dQejSrhPVnYKptX5KlApsl4Zf4S"
        access_token_secret = "mJcFCSqnA8mnjVaD0xMnJIIDItHjVeMiv6JVSe9XrAUzf"
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        self.api = tweepy.API(auth)

    def next_tuple(self):
        # test delay
        time.sleep(5)

        search_results = self.api.search("london")

        for tweet in search_results:
            try:
                text = tweet.text
                if text:
                    tweet_id = hl.md5(text.encode('ascii', 'replace')).hexdigest()

                    # log
                    log.debug("LineSpout: " + text)
                    try:
                        self.client.log.tweets.insert(
                            dict(
                                _id=tweet_id,
                                text=text,
                                at=datetime.now()
                            )
                        )
                        # emit
                        self.emit((text, tweet_id), tup_id=str(uuid4()))
                    except pyerrors.DuplicateKeyError:
                        pass
            except:
                e = sys.exc_info()[0]
                print "Error: %s" % e
                traceback.print_exc(file=sys.stdout)


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.DEBUG,
        filename='/tmp/word_count_lines.log',
        format="%(message)s",
        filemode='a',
    )

    TweetSpout().run()
