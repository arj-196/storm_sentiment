from collections import defaultdict
from collections import namedtuple
import logging
from pymongo import MongoClient
from uuid import uuid4
from datetime import datetime
from py4j.java_gateway import JavaGateway, GatewayParameters
from pyleus.storm import SimpleBolt

log = logging.getLogger('counter')

Counter = namedtuple('Counter', 'word count')


class SentimentAnalyzerBolt(SimpleBolt):
    OUTPUT_FIELDS = Counter
    bolt_id = None
    client = None
    analyzer = None

    def initialize(self):
        self.bolt_id = str(uuid4())
        self.client = MongoClient('localhost')
        gateway = JavaGateway(
            gateway_parameters=GatewayParameters(port=25333)
        )
        self.analyzer = gateway.entry_point.getCache(self.bolt_id)

    def process_tuple(self, tup):
        tweet, tweet_id = tup.values
        log.debug("CountWordsBolt: Tuple Values: " + tweet + " : " + tweet_id)

        # get sentiment
        r = self.analyzer.analyze(tweet)
        sentiment = r.getSentiment()

        self.client.log.tweets.update_one(
            {'_id': tweet_id},
            {'$set': {'sentiment': sentiment}}
        )


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.DEBUG,
        filename='/tmp/word_count_lines.log',
        format="%(message)s",
        filemode='a',
    )

    SentimentAnalyzerBolt().run()
